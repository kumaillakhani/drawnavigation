package pk.keenu.mylocation;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.*;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.View;

public class NavActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleMap.OnCameraChangeListener {

    private GoogleMap mMap;
    String markerTitle;
    Double FromLat, FromLon, ToLat, ToLon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getIntent().getExtras();
        markerTitle= b.getString("markertitle");
        FromLat= b.getDouble("FromLat");
        FromLon= b.getDouble("FromLon");
        ToLat= b.getDouble("ToLat");
        ToLon= b.getDouble("ToLon");

        setContentView(R.layout.activity_nav);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


//    /**
//     * Manipulates the map once available.
//     * This callback is triggered when the map is ready to be used.
//     * This is where we can add markers or lines, add listeners or move the camera. In this case,
//     * we just add a marker near Sydney, Australia.
//     * If Google Play services is not installed on the device, the user will be prompted to install
//     * it inside the SupportMapFragment. This method will only be triggered once the user has
//     * installed Google Play services and returned to the app.
//     */
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//
//        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//    }

    @Override
    public void onCameraChange(CameraPosition arg0) {
        try {
//            getView().findViewById(R.id.setAddress_layoutOnMap).setVisibility(View.INVISIBLE);
//
//            mCameraHandler.removeCallbacks(mCameraMoveCallback);
//            mCameraHandler.postDelayed(mCameraMoveCallback, 1500);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onMapReady(GoogleMap arg0) {
        try {
            mMap = arg0;
            mMap.setBuildingsEnabled(false);
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            }

            mMap.setOnCameraChangeListener(this);

            GPSTracker gps = new GPSTracker(this);
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15);
            mMap.animateCamera(update);

            String[] params = new String[2];
//            String fromString = "24.8564077,67.0167615";
//            String toString = "24.8640251,67.0262035";
            String fromString = FromLat+","+FromLon;
            String toString = ToLat+","+ToLon;
            params[0] = fromString;
            params[1] = toString;
            GetGoogleDirections direction = new GetGoogleDirections(this, mMap, false);
            direction.execute(params);

        } catch (Exception e) {
            e.printStackTrace();
            try {
                LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
                String provider = manager.getBestProvider(new Criteria(), true);

                if (provider != null) {
                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        Location loc = manager.getLastKnownLocation(provider);
                        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(loc.getLatitude(), loc.getLongitude()), 15);
                        mMap.animateCamera(update);
                    }
                }

            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }
}
