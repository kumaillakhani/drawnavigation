package pk.keenu.mylocation;

import android.*;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnMarkerClickListener {

    Context mContext = MapsActivity.this;
    Button butnGetLocation;
    GPSTracker gps;
    double currentLatitude, currentLongitude;


    Location mLastLocation;
    GoogleMap mGoogleMap;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    Marker mCurrLocationMarker;
    SupportMapFragment mapFrag;

    private Marker myMarker;

    /**
     * @author: Kumail Raza Lakhani
     * For requesting Location permission in Android 6.0 or above.
     */
    private static final String[] INITIAL_PERMS={
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final String[] LOCATION_PERMS={
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final int INITIAL_REQUEST=1337;
    private static final int LOCATION_REQUEST=INITIAL_REQUEST+1;
    int REQUEST_LOCATION;

    /*
     * End's --->
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

/**
 *  Request Location permission in Android 6.0 or above
 */
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            if (!canAccessLocation()) {
                RequestPopup();
            } else {
                GetDeviceLocation();
            }
        } else {
            GetDeviceLocation();
        }
    }

    /**
     *  Get device location.
     */
    public void GetDeviceLocation() {
        try {
            gps = new GPSTracker(mContext);
            if (gps.canGetLocation()) {

                currentLatitude = gps.getLatitude(); // returns latitude
                currentLongitude = gps.getLongitude(); // returns longitude

//                String uri = "http://maps.google.com/maps?f=d&hl=en&saddr=" + currentLatitude + "," + currentLongitude + "&daddr=" + obj.branchLatitude + "," + obj.branchLongitude;
//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
//                startActivity(Intent.createChooser(intent, "Select an application"));
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/Network in settings
                gps.showSettingsAlert();
            }
        } catch (Exception e) {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/Network in settings
            gps.showSettingsAlert();
            Toast.makeText(mContext, "Error", Toast.LENGTH_LONG);
        }
    }
/**
 *  Get device End.
 */

    /**
     *  Popup Location permission in Android 6.0 or above.
     */
    public void RequestPopup() {
        AlertDialog.Builder builder =  new  AlertDialog.Builder(this);
        builder.setTitle("Location Permission")
                .setMessage("Location access is required to use this app.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
//                                    requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
                        requestPermissions(new String[]{
                                        android.Manifest.permission.ACCESS_FINE_LOCATION,
                                        android.Manifest.permission.ACCESS_COARSE_LOCATION},
                                REQUEST_LOCATION);
                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }
/**
 *  Popup Location permission in Android 6.0 or above. END ->
 */


    /**
     *  Request Location permission in Android 6.0 or above.
     */
    private boolean canAccessLocation() {
        return(hasPermission(android.Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return(PackageManager.PERMISSION_GRANTED== ContextCompat.checkSelfPermission(this, perm));
    }

    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            if(grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // We can now safely use the API we requested access to
                GetDeviceLocation();
            } else {
                // Permission was denied or request was cancelled
                RequestPopup();
            }
        }
    }
    /**
     *  Request Location permission in Android 6.0 or above. END ->
     */


    @Override
    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mGoogleMap=googleMap;
//        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
            } else {
                //Request Location Permission
//                checkLocationPermission();
                RequestPopup();
            }
        }
        else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {}

    @Override
    public void onLocationChanged(Location location)
    {
        myMarker = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(24.875808, 67.022974)).title("Karachi Zoo."));
//        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(24.868994, 67.031729)).title("Daewoo City Terminal."));
        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(24.858579, 67.014241)).title("Jama Cloth Market."));
        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(24.869631, 67.030753)).title("Taj Medical Complex."));
        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(24.871247, 67.039808)).title("Mazar E Quaid."));


        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("You are here.");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);

        //move map camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 14));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);

        //optionally, stop location updates if only current location is needed
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

//        mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
//            @Override
//            public void onInfoWindowClick(Marker marker) {
//                Intent intent = new Intent(MapsActivity.this, NavActivity.class);
//
//                GPSTracker gps = new GPSTracker(MapsActivity.this);
//                double FromLat, FromLon, ToLat, ToLon;
//                FromLat = gps.getLatitude();
//                FromLon = gps.getLongitude();
//                ToLat = 0.0;
//                ToLon = 0.0;
//
//                String title = marker.getTitle();
//
//                if(title.equalsIgnoreCase("Karachi Zoo.")){
//                    ToLat = marker.getPosition().latitude;
//                    ToLon = marker.getPosition().longitude;
//                } else if(title.equalsIgnoreCase("Jama Cloth Market.")) {
//                    ToLat = marker.getPosition().latitude;
//                    ToLon = marker.getPosition().longitude;
//                } else if(title.equalsIgnoreCase("Taj Medical Complex.")) {
//                    ToLat = marker.getPosition().latitude;
//                    ToLon = marker.getPosition().longitude;
//                } else if(title.equalsIgnoreCase("Mazar E Quaid.")) {
//                    ToLat = marker.getPosition().latitude;
//                    ToLon = marker.getPosition().longitude;
//                }
//
//                intent.putExtra("markertitle", title);
//                intent.putExtra("FromLat", FromLat);
//                intent.putExtra("FromLon", FromLon);
//                intent.putExtra("ToLat", ToLat);
//                intent.putExtra("ToLon", ToLon);
//                startActivity(intent);
//            }
//        });

        mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
        mGoogleMap.setOnMarkerClickListener(this);
    }

    @Override
    public boolean onMarkerClick(Marker marker) { //Called when a marker has been clicked or tapped.

        Intent intent = new Intent(MapsActivity.this, NavActivity.class);

        GPSTracker gps = new GPSTracker(MapsActivity.this);
        double FromLat, FromLon, ToLat, ToLon;
        FromLat = gps.getLatitude();
        FromLon = gps.getLongitude();
        ToLat = 0.0;
        ToLon = 0.0;

        String title = marker.getTitle();

        if(title.equalsIgnoreCase("Karachi Zoo.")){
            ToLat = marker.getPosition().latitude;
            ToLon = marker.getPosition().longitude;
        } else if(title.equalsIgnoreCase("Jama Cloth Market.")) {
            ToLat = marker.getPosition().latitude;
            ToLon = marker.getPosition().longitude;
        } else if(title.equalsIgnoreCase("Taj Medical Complex.")) {
            ToLat = marker.getPosition().latitude;
            ToLon = marker.getPosition().longitude;
        } else if(title.equalsIgnoreCase("Mazar E Quaid.")) {
            ToLat = marker.getPosition().latitude;
            ToLon = marker.getPosition().longitude;
        }

        intent.putExtra("markertitle", title);
        intent.putExtra("FromLat", FromLat);
        intent.putExtra("FromLon", FromLon);
        intent.putExtra("ToLat", ToLat);
        intent.putExtra("ToLon", ToLon);
        startActivity(intent);
        return false;
    }

    public void GetDirections(String[] cordinates) {
//        String[] params = new String[2];
//        params[0] = cordinates[0];
//        params[1] = cordinates[1];
        GetGoogleDirections direction = new GetGoogleDirections(this, mGoogleMap, false);
        direction.execute(cordinates);
    }

//    @Override
//    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//
//    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//
//    }
}


//implements OnMapReadyCallback {
//
//    private GoogleMap mMap;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_maps);
//        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
//    }
//
//
//    /**
//     * Manipulates the map once available.
//     * This callback is triggered when the map is ready to be used.
//     * This is where we can add markers or lines, add listeners or move the camera. In this case,
//     * we just add a marker near Sydney, Australia.
//     * If Google Play services is not installed on the device, the user will be prompted to install
//     * it inside the SupportMapFragment. This method will only be triggered once the user has
//     * installed Google Play services and returned to the app.
//     */
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//
//        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//    }
//}
