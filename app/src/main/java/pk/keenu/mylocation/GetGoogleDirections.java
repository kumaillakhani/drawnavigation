package pk.keenu.mylocation;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class GetGoogleDirections extends AsyncTask<String[], Void, String> {
	private static final String GOOGLE_BASE_URL = "https://maps.googleapis.com/maps/api/directions/json?";

	private static final String GOOGLE_PARAM_ORIGIN = "origin=";
	private static final String GOOGLE_PARAM_APIKEY = "key=";
	private static final String GOOGLE_PARAM_DESTINATION = "destination=";
	private static final String GOOGLE_PARAM_WAYPOINTS = "waypoints=";

	private ProgressDialog mDialog;
	private GoogleMap mMapDirections;
	private Context mContext;
	private ArrayList<LatLng> mViaPoints;
	private boolean isTracking;
	private String GOOGLE_API_KEY = "AIzaSyDEuJCsRCR1SbNws5xrcjwI2NYpzGcBSzU";

	public GetGoogleDirections(Context context, GoogleMap map, boolean isTracking) {
		mContext = context;
		mMapDirections = map;
		this.isTracking = isTracking;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		try {
			mDialog = new ProgressDialog(mContext);
			mDialog.setTitle("Getting Directions");
			mDialog.setMessage("Please Wait");
			mDialog.setCancelable(false);
			mDialog.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		if (result != null && !result.isEmpty()) {
			try {
				JSONObject json = new JSONObject(result);
				if (json.getString("status").equals("OK")) {

					MarkerOptions from = new MarkerOptions();
//					if (this.isTracking)
//						from.icon(BitmapDescriptorFactory.fromResource(R.drawable.from));
//					else
//						from.icon(BitmapDescriptorFactory.fromResource(R.drawable.from));
					from.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));

					MarkerOptions to = new MarkerOptions();
//					if (this.isTracking)
//						to.icon(BitmapDescriptorFactory.fromResource(R.drawable.to));
//					else
//						to.icon(BitmapDescriptorFactory.fromResource(R.drawable.to));
					to.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));

					DirectionsJSONParser parser = new DirectionsJSONParser();
					List<List<HashMap<String, String>>> data = parser.parse(json);
					String milage = parser.parseEstimate(json);

					if (!milage.isEmpty() && mMilageListener != null)
						mMilageListener.setMilage(milage);

					try {

						PolylineOptions lineOptions = new PolylineOptions();
						LatLngBounds.Builder builder = LatLngBounds.builder();

						lineOptions.width(4);
						lineOptions.color(0xFF000000);

						// Use only one response route. because requested
						// query contain via:
						// so no legs are created.. Read google guide for
						// Maps Direction API for more details
						for (int i = 0; i < data.get(0).size(); i++) {
							try {
								double lat = Double.parseDouble(data.get(0).get(i).get("lat"));
								double lng = Double.parseDouble(data.get(0).get(i).get("lng"));
								LatLng position = new LatLng(lat, lng);

								lineOptions.add(position);
								builder.include(position);
							} catch (OutOfMemoryError e) {
								e.printStackTrace();
							} catch (Exception ex) {
								ex.printStackTrace();
							}
						}

						double lat = Double.parseDouble(data.get(0).get(0).get("lat"));
						double lng = Double.parseDouble(data.get(0).get(0).get("lng"));
						LatLng positionFrom = new LatLng(lat, lng);

						from.position(positionFrom);
						to.position(lineOptions.getPoints().get(lineOptions.getPoints().size() - 1));

						this.mMapDirections.addPolyline(lineOptions);
						this.mMapDirections.addMarker(from);
						this.mMapDirections.addMarker(to);

//						if (mViaPoints != null && !mViaPoints.isEmpty()) {
//							for (LatLng field : mViaPoints) {
//								MarkerOptions via = new MarkerOptions();
//								via.icon(BitmapDescriptorFactory.fromResource(R.drawable.via));
//								via.position(field);
//								mMapDirections.addMarker(via);
//							}
//						}
						try {
							CameraUpdate update = CameraUpdateFactory.newLatLngBounds(builder.build(),
									(int) mContext.getResources().getDimension(R.dimen.maps_padding));
							mMapDirections.animateCamera(update);
						} catch (IllegalStateException e) {
							e.printStackTrace();
							CameraUpdate update = CameraUpdateFactory.newLatLngBounds(builder.build(), 800, 400, 0);
							mMapDirections.animateCamera(update);
						}

					} catch (NullPointerException | IllegalStateException e) {
						e.printStackTrace();
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			Toast.makeText(mContext, "Problem in connection to Google. Please check your Internet Connection", Toast.LENGTH_SHORT).show();
		}

		if (mDialog != null)
			mDialog.dismiss();
	}

	@Override
	protected String doInBackground(String[]... params) {
		StringBuilder builder = new StringBuilder();
		builder.append(GOOGLE_BASE_URL);

		String[] addresses = params[0];

		if (addresses != null && addresses.length >= 2) {
			mViaPoints = new ArrayList<LatLng>();
			boolean hasAddedWayPoint = false;
			for (int i = 0; i < addresses.length; i++) {
				switch (i) {
				case 0:
					builder.append(GOOGLE_PARAM_ORIGIN + addresses[i]);
					break;
				case 1:
					builder.append("&" + GOOGLE_PARAM_DESTINATION + addresses[i]);
					break;
				default:
					if (!hasAddedWayPoint) {
						builder.append("&" + GOOGLE_PARAM_WAYPOINTS);
						hasAddedWayPoint = true;
					}
					builder.append(addresses[i]);
					if (i != addresses.length - 1)
						builder.append("|via:");

					try {
						if (addresses[i].contains(",")) {
							String[] splitted = addresses[i].split(",");
							if (splitted != null && splitted.length >= 2) {
								double lat = Double.parseDouble(splitted[0]);
								double lon = Double.parseDouble(splitted[1]);

								mViaPoints.add(new LatLng(lat, lon));
							}
						}
					} catch (NumberFormatException e) {
						e.printStackTrace();
					}
					break;
				}
			}

			builder.append("&" + GOOGLE_PARAM_APIKEY + GOOGLE_API_KEY);

			try {

				HttpsURLConnection conn = (HttpsURLConnection) new URL(builder.toString()).openConnection();
				InputStream stream = conn.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

				String line = null;
				StringBuilder response = new StringBuilder();

				while ((line = reader.readLine()) != null)
					response.append(line).append("\n");

				return response.toString();
			} catch (IOException e) {
				e.printStackTrace();

			}

		}

		return null;
	}

	private GoogleDirectionsMilageCallback mMilageListener;

	public void setMilageListener(GoogleDirectionsMilageCallback callback) {
		mMilageListener = callback;
	}

	public interface GoogleDirectionsMilageCallback {
		public abstract void setMilage(String text);
	}
}