package pk.keenu.mylocation;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends AppCompatActivity
        implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    Context mContext = MainActivity.this;
    Intent intent;
    Button butnGoToLocation, butnGetLocations;
    Spinner spinner;
    GPSTracker gps;
    double currentLatitude, currentLongitude;
    Data[] objData;

    Location mLastLocation;
    GoogleMap mGoogleMap;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    Marker mCurrLocationMarker;
    SupportMapFragment mapFrag;

    /**
     * @author: Kumail Raza Lakhani
     * For requesting Location permission in Android 6.0 or above.
     */
    private static final String[] INITIAL_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final String[] LOCATION_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final int INITIAL_REQUEST=1337;
    private static final int LOCATION_REQUEST=INITIAL_REQUEST+1;
    int REQUEST_LOCATION;

    /*
     * End's --->
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        // put values into map
//        map.put("jama", jama);
//        map.put("taj", taj);
//        map.put("zoo", zoo);
//        map.put("quaid", quaid);

//        mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
//        mapFrag.getMapAsync(this);

/**
 *  Request Location permission in Android 6.0 or above
 */

        String[] location = {"jama", "taj", "zoo", "quaid"};
        double[] latList = {24.858579, 24.868274, 24.875808, 24.874128};
        double[] lonList = {67.014241, 67.030764, 67.022974, 67.039744};

        int length = 4;

        objData = new Data[length];

        for (int i = 0; i < length; i++) {
            objData[i] = new Data();
            objData[i].latitude = latList[i];
            objData[i].longitude = lonList[i];
        }

//        mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
//        mapFrag.getMapAsync(this);

/**
 *  Request Location permission in Android 6.0 or above
 */
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            if (!canAccessLocation()) {
                RequestPopup();
            } else {
                GetDeviceLocation();
            }
        } else {
            GetDeviceLocation();
        }

        butnGetLocations = (Button) findViewById(R.id.butnGetLocations);
        butnGetLocations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity.this, MapsActivity.class);
//                intent = new Intent(MainActivity.this, NavActivity.class);
                startActivity(intent);
            }
        });

        butnGoToLocation = (Button) findViewById(R.id.butnGoToLocation);
        butnGoToLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int index = Integer.parseInt(butnGoToLocation.getTag().toString());

                if (index==0){
                    String destLat = String.valueOf(objData[index].latitude);
                    String destLong = String.valueOf(objData[index].longitude);
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?saddr="+currentLatitude+","+currentLongitude+"&daddr="+destLat+","+destLong));
                } else if (index==1){
                    String destLat = String.valueOf(objData[index].latitude);
                    String destLong = String.valueOf(objData[index].longitude);
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?saddr="+currentLatitude+","+currentLongitude+"&daddr="+destLat+","+destLong));
                } else if (index==2){
                    String destLat = String.valueOf(objData[index].latitude);
                    String destLong = String.valueOf(objData[index].longitude);
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?saddr="+currentLatitude+","+currentLongitude+"&daddr="+destLat+","+destLong));
                } else {
                    String destLat = String.valueOf(objData[index].latitude);
                    String destLong = String.valueOf(objData[index].longitude);
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?saddr="+currentLatitude+","+currentLongitude+"&daddr="+destLat+","+destLong));
                }
                startActivity(intent);

//                GetDeviceLocation();
//                Intent i = new Intent(MainActivity.this, NavActivity.class);
//                startActivity(i);

//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                        Uri.parse("http://maps.google.com/maps?saddr=24.858579,67.014241&daddr=24.869631,67.030753"));
//                startActivity(intent);
            }
        });


        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, location); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                butnGoToLocation.setTag(position+""); // Passing as string
                parentView.getItemAtPosition(position);
                parentView.setSelection(position);
                parentView.getSelectedItemPosition();

//                String destLat = String.valueOf(objData[position].latitude);
//                String destLong = String.valueOf(objData[position].longitude);
//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                        Uri.parse("http://maps.google.com/maps?saddr="+currentLatitude+","+currentLongitude+"&daddr="+destLat+","+destLong));
//                startActivity(intent);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

/**
 *  Get device location.
 */
    public void GetDeviceLocation() {
        try {
            gps = new GPSTracker(mContext);
            if (gps.canGetLocation()) {

                currentLatitude = gps.getLatitude(); // returns latitude
                currentLongitude = gps.getLongitude(); // returns longitude

//                String uri = "http://maps.google.com/maps?f=d&hl=en&saddr=" + currentLatitude + "," + currentLongitude + "&daddr=" + obj.branchLatitude + "," + obj.branchLongitude;
//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
//                startActivity(Intent.createChooser(intent, "Select an application"));
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/Network in settings
                gps.showSettingsAlert();
            }
        } catch (Exception e) {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/Network in settings
            gps.showSettingsAlert();
            Toast.makeText(mContext, "Error", Toast.LENGTH_LONG);
        }
    }
/**
 *  Get device End.
 */

/**
 *  Popup Location permission in Android 6.0 or above.
 */
    public void RequestPopup() {
        AlertDialog.Builder builder =  new  AlertDialog.Builder(this);
        builder.setTitle("Location Permission")
                .setMessage("Location access is required to use this app.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
//                                    requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
                        requestPermissions(new String[]{
                                        Manifest.permission.ACCESS_FINE_LOCATION,
                                        Manifest.permission.ACCESS_COARSE_LOCATION},
                                REQUEST_LOCATION);
                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }
/**
 *  Popup Location permission in Android 6.0 or above. END ->
 */


/**
 *  Request Location permission in Android 6.0 or above.
 */
    private boolean canAccessLocation() {
        return(hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return(PackageManager.PERMISSION_GRANTED== ContextCompat.checkSelfPermission(this, perm));
    }

    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            if(grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // We can now safely use the API we requested access to
                GetDeviceLocation();
            } else {
                // Permission was denied or request was cancelled
                RequestPopup();
            }
        }
    }
/**
 *  Request Location permission in Android 6.0 or above. END ->
 */


@Override
public void onPause() {
    super.onPause();

    //stop location updates when Activity is no longer active
    if (mGoogleApiClient != null) {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }
}

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mGoogleMap=googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
            } else {
                //Request Location Permission
//                checkLocationPermission();
                RequestPopup();
            }
        }
        else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {}

    @Override
    public void onLocationChanged(Location location)
    {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("You are here.");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);

        //move map camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(16));

        //optionally, stop location updates if only current location is needed
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

//    @Override
//    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//
//    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//
//    }

    private class Data {

        public double longitude;
        public double latitude;

//        Data(){
//            longitude = this.latitude;
//            latitude = this.longitude;
//        }

    }
}
